import { Controller, Get, Param, Post, Req, Res, UploadedFile, UseGuards, UseInterceptors, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileService } from '../../services/file/file.service';

@Controller('files')
export class FilesController {

  constructor(
    private readonly _file: FileService,
  ) { }

  @Post('files')
  @UseGuards( AuthGuard() )
  @UseInterceptors(FileInterceptor('file'))
  async addPrivateFile(@Request() req: any, @Res() res, @UploadedFile() file: Express.Multer.File) {
    return this._file.uploadPrivateFile(req.user, res, file.buffer, file.originalname);
  }

  @Get('files/:id')
  @UseGuards( AuthGuard() )
  async getPrivateFile(@Res() res, @Param() { id }) {
    return await this._file.getPrivateFile(res, id);
  }

  @Get('filesAll')
  @UseGuards( AuthGuard() )
  async getAllFilesAws(@Res() res) {
    return await this._file.getAllFilesAws(res);
  }

}
