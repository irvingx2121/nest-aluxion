import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Res } from '@nestjs/common';
import { CreateUserDto, LoginUserDto } from '../../dto';
import { AuthenticateService } from '../../services/authenticate/authenticate.service';

@Controller('authenticate')
export class AuthenticateController {

  constructor(
    private readonly _authenticate: AuthenticateService
  ) { }

  @Post('register')
  createUser(@Res() res, @Body() createUserDto: CreateUserDto) {
    return this._authenticate.create(res, createUserDto);
  }

  @Post('login')
  loginUser(@Res() res, @Body() loginUserDto: LoginUserDto) {
    return this._authenticate.login(res, loginUserDto);
  }


}
