import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity('file_user')
export class Files {

    @PrimaryGeneratedColumn('uuid')
    id?: string;

    @Column()
    key: string;

    @ManyToOne(() => User, (owner: User) => owner.files)
    user_id: User;
}
