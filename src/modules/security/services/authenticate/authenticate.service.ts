import { BadRequestException, InternalServerErrorException, Logger, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../domain/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../../interfaces/jwt-payload.interface';
import { CreateUserDto } from '../../dto/create-user.dto';
import { Response } from 'express';
import { Respuesta } from 'src/utils/respuesta';
import { LoginUserDto } from '../../dto';

@Injectable()
export class AuthenticateService {

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly jwtService: JwtService,
    ) { }

    async create(res: Response, createUserDto: CreateUserDto) {
        try {
            const { email, password, fullName } = createUserDto;
            const emailExist = await this.userRepository.findBy({ email: email });
            if (emailExist.length > 0) {
                return res.status(HttpStatus.BAD_REQUEST).json(new Respuesta(
                    HttpStatus.BAD_REQUEST,
                    null,
                    'El correo ya se encuentra en uso.'));
            }
            const user = this.userRepository.create({ email, fullName, password: bcrypt.hashSync(password, 10)});
            await this.userRepository.save(user)
            delete user.password;
            return res.status(HttpStatus.OK).json(new Respuesta(
                HttpStatus.OK,
                { user: user, token: this.getJwtToken({ id: user.id })},
                'Se realizó el registro correctamente'
            ));
        } catch (error) {
            Logger.log(error)
            this.handleDBErrors(error);
        }
    }

    async login(res: Response, loginUserDto: LoginUserDto ) {
        const { password, email } = loginUserDto;
        const user = await this.userRepository.findOne({ where: { email }, select: { email: true, password: true, id: true }});
        if ( !user ) throw new UnauthorizedException('Las credenciales no son válidas (email)');
    
        if ( !bcrypt.compareSync( password, user.password ) ) throw new UnauthorizedException('Las credenciales no son válidas (password)');
        
        delete user.password;
        return res.status(HttpStatus.OK).json(new Respuesta(
            HttpStatus.OK,
            { user: user, token: this.getJwtToken({ id: user.id })},
            'Login se realizó correctamente'
        ));
      }
      
    private getJwtToken(payload: JwtPayload): string {
        return this.jwtService.sign(payload);
    }

    private handleDBErrors(error: any): never {
        if (error.code === '23505') {
            throw new BadRequestException(error.detail);
        }
        throw new InternalServerErrorException('Please check server logs');
    }


}
