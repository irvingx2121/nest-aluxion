import { Injectable, NotFoundException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Files } from '../../domain/files.entity';
import { Repository } from 'typeorm';
import { S3 } from 'aws-sdk';
import { v4 as uuid } from 'uuid';
import { ConfigService } from '@nestjs/config';
import { User } from '../../domain/user.entity';
import { Response } from 'express';
import { Respuesta } from 'src/utils/respuesta';

@Injectable()
export class FileService {

  s3: any = {};

  constructor(
    @InjectRepository(Files)
    private filesRepository: Repository<Files>,
    private readonly configService: ConfigService
  ) {
    this.s3 = new S3({
      accessKeyId: this.configService.get('AWS_PRIVATE_KEY_ID'),
      secretAccessKey: this.configService.get('AWS_PRIVATE_SECRECT_KEY')
    });
  }

  async uploadPrivateFile(user: User, res: Response, dataBuffer: Buffer, filename: string) {
    const uploadResult = await this.s3.upload({
      Bucket: this.configService.get('AWS_PRIVATE_BUCKET_NAME'),
      Body: dataBuffer,
      Key: `${uuid()}-${filename}`
    }).promise();

    const newFile = this.filesRepository.create({
      key: uploadResult.Key,
      user_id: { id: user.id }
    });

    await this.filesRepository.save(newFile);
    return res.status(HttpStatus.OK).json((new Respuesta(
      HttpStatus.OK,
      newFile,
      'Se realizó el registro correctamente'
    )));
  }

  public async getAllFilesAws(res: Response) {
    try {
      const uploadResult = await this.s3.listObjects({
        Bucket: this.configService.get('AWS_PRIVATE_BUCKET_NAME')
      }).promise();
      return res.status(HttpStatus.OK).json((new Respuesta(
        HttpStatus.OK,
        uploadResult,
        'Se realizó la consulta correctamente'
      )));
    } catch (error) {
      throw new NotFoundException();
    }
  }

  public async getPrivateFile(res: Response, fileId: string) {
    try {
      const file = await this.filesRepository.findOneBy({ id: fileId });
      if (file === null) {
        return res.status(HttpStatus.BAD_REQUEST).json((new Respuesta(
          HttpStatus.BAD_REQUEST,
          null,
          'No se encontraron registros.'
        )));
      }

      const stream = await this.s3.getObject({
        Bucket: this.configService.get('AWS_PRIVATE_BUCKET_NAME'),
        Key: file.key
      }).createReadStream();

      return res.status(HttpStatus.OK).json((new Respuesta(
        HttpStatus.OK,
        { info: file, url: await this.generatePresignedUrl(file.key) },
        'Se realizó la consulta correctamente'
      )));
      
    } catch (error) {
      throw new NotFoundException();
    }
  }

  public async generatePresignedUrl(key: string) {
    return this.s3.getSignedUrlPromise('getObject', {
      Bucket: this.configService.get('AWS_PRIVATE_BUCKET_NAME'),
      Key: key
    })
  }
}
