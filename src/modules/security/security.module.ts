import { Logger, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticateController } from './controller/authenticate/authenticate.controller';
import { User } from './domain/user.entity';
import { AuthenticateService } from './services/authenticate/authenticate.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FileService } from './services/file/file.service';
import { Files } from './domain/files.entity';
import { FilesController } from './controller/files/files.controller';

@Module({
  controllers: [AuthenticateController, FilesController],
  providers: [AuthenticateService, JwtStrategy, FileService],
  imports: [
    ConfigModule,

    TypeOrmModule.forFeature([ User, Files ]),
    PassportModule.register({ defaultStrategy: 'jwt' }),

    JwtModule.registerAsync({
      imports: [ ConfigModule ],
      inject: [ ConfigService ],
      useFactory: ( configService: ConfigService ) => {
        console.log('Enviroments');
        console.log('JWT Secret', configService.get('JWT_SECRET') )
        console.log('JWT SECRET', process.env.JWT_SECRET)
        Logger.log('JWT Secret', configService.get('JWT_SECRET'));
        return {
          secret: configService.get('JWT_SECRET'),
          signOptions: {
            expiresIn:'2h'
          }
        }
      }
    })
  ],
  exports: [ TypeOrmModule, JwtStrategy, PassportModule, JwtModule ]
})
export class SecurityModule {}
