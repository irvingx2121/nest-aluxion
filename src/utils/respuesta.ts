export class Respuesta {
    
    private statusCode: number;
    private response: any;
    private message: string;
    private error?: string;

    constructor(status: number, response: any, message: string, error?: string) {
        this.statusCode = status;
        this.response = response;
        this.message = message;
        this.error = error;
    }
    
}
