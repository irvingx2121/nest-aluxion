import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SecurityModule } from './modules/security/security.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { TranslatorModule } from 'nestjs-translator';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      database: process.env.DB_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      autoLoadEntities: true,
      synchronize: true,
      ssl: {
        rejectUnauthorized: false
      },
      extra: {
        NODE_TLS_REJECT_UNAUTHORIZED: 0
      },
    }),
    TranslatorModule.forRoot({
      global: true,
      defaultLang: 'es',
      translationSource: '/src/i18n'
    }),
    SecurityModule
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: []
})
export class AppModule { }
